# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# Infinite history
HISTSIZE= HISTFILESIZE=
HISTIGNORE="ls -rtlh[a]:cd:upg:alias:c:l:[bf]g:cl"

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
shopt -s globstar

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
  if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
  # We have color support; assume it's compliant with Ecma-48
  # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
  # a case would tend to support setf rather than setaf.)
  color_prompt=yes
  else
  color_prompt=
  fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w \$\[\033[00m\] '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
  test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
  alias ls='ls --color=auto'
  alias grep='grep --color=auto'
  alias fgrep='fgrep --color=auto'
  alias egrep='egrep --color=auto'
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Aliases

alias a='cd ${HOME}/Lab/ADMINISTRATIF && ls -rtlh'
alias agr='alias | grep -i'
alias b='bpytop'
alias bc='bc -ql'
alias c='cd ${HOME}/.config && ls -rtlha'
alias cdsh='cd ${HOME}/Lab/SHELLSCRIPTS && ls -rtlh'
alias cdssh='cd ${HOME}/.ssh && ls -rtlh'
alias cl='clear;ls -rtlh'
alias d='cd ${HOME}/Lab/DOCUMENTATION && ls -rtlh'
alias dmesg='dmesg -T'
alias duree='for f in *; do ffmpeg -i "$f" 2>&1 | grep Duration | cut -d " " -f 4 | sed s/,// | cut -d . -f1 | tr -d "\n" && echo " - $f"; done | grep -v ".srt"'   # exiftool -T -Duration -FileName
alias e='espeak'
alias edb='vi ${HOME}/.bashrc'
alias f='find .'
alias ff='find . -type f -iname'
alias free='free -mth'
alias g='cd ${HOME}/Lab/GAMING && ls -rtlh'
alias gg='cd ${HOME}/Lab/GITLAB && ls -rtlh'
alias grep2="grep -v '^\s*$\|^\s*\#' $1"
alias grepip='egrep -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}"'
alias h='history'
alias htop='htop -t'
alias i='identify'
alias ins='sudo apt install -y'
alias ip='ip -c -brief addr'
alias iws='iwlist wlan0 scan'
alias j='jobs -l'
alias jo="journalctl -p 3 -xb"
alias k='khal calendar'
alias l='ls -rtlh'
alias la='ls -rtlha'
alias lab='cd ${HOME}/Lab && ls -rtlh'
alias lo='cd ${HOME}/Lab/LOGS && ls -rtlh'
alias lsssh='ls -rtlh ${HOME}/.ssh'
alias lst='cd ${HOME}/Téléchargements && ls -rtlh'
alias m='cd ${HOME}/Musique && ls -rtlh'
alias manila='var=$(TZ='Asia/Manila' date +%Hh%M); echo "Oras sa Manila : ${var}"'
alias max='cd /media/${USER}/Maxtor && ls -rtlh'
alias meteos="curl -s 'http://wttr.in/{Manila,Tokyo,Bangkok,Seoul,Oslo,Helsinki,Montreal}?format=3'"
alias mount='mount | column -t'
alias n='nmap -sn 192.168.1.0/24'
alias octal="stat -c '%a %A %n'"
alias p='cd ${HOME}/Lab/ADMINISTRATIF/PAMILYA && ls -rtlh'
alias q='qutebrowser'
alias r='ranger'
alias rem='sudo apt remove --purge -y'
alias s='sxiv'
alias sc='du -ms ${HOME}/.cache/chromium/'
alias sch='apt search'
alias siwd='sudo ifconfig wlan0 down'
alias siwu='sudo ifconfig wlan0 up'
alias sl='ls'
alias sm='cd ${HOME}/Lab/SHELLSCRIPTS/MISC && ls -rtlh'
alias sob='source ${HOME}/.bashrc'
alias svp='sudo $(history -p !-1)'
alias t='tty-clock -c -f %d-%m-%Y'
alias tokyo='var=$(TZ='Asia/Tokyo' date +%Hh%M); echo "A Tokyo, il est ${var}"'
alias topmem='ps aux | awk '\''{print $6/1024 " MB\t\t" $11}'\'' | sort -rn | head -20'
alias u='sudo apt update && apt list --upgradable'
alias upg='sudo apt upgrade -y'
alias v='cd ${HOME}/Vidéos && ls -rtlh'
alias va='cd ${HOME}/Vidéos/ANIME && ls -rtlh'
alias vd='cd "$(ls -rt1 | tail -1)" && ls -rtlh'
alias vf='cd ${HOME}/Vidéos/FILMS && ls -rtlh'
alias vi='vim'
alias vida='vim ${HOME}/Lab/DASHBOARD'
alias vm='cd ${HOME}/Vidéos/MISC && ls -rtlh'
alias vp='cd "$(ls -rt1 | head -1)" && ls -rtlh'
alias vv='vi ${HOME}/.vimrc'
alias wl='cat ~/.vim/misc/wordlist'
alias xrags='xargs'
alias y="youtube-dl -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best'"
alias z='zathura'

# I3wm

alias i3c='vi ${HOME}/.config/i3/config'
alias i3s='vi ${HOME}/.config/i3status/config'
alias xx='xrdb ${HOME}/.Xresources'
alias vixx='vi ${HOME}/.Xresources'

# Git aliases

alias gs="git status"
alias ga="git add"
alias gcm="git commit -m"
alias gc="git checkout"
alias gp="git push"
alias gpl="git pull"
alias gb="git branch"
alias gd="git diff"
alias gl="git log --date=format:'%d-%m-%Y %H:%M:%S' --pretty=format:'%Cred%h - %Cblue%an - %Cgreen%ad : %C(yellow)%s'"

# Pack and unpack tar.gz files:
alias tarup="tar -zcf"
alias tardown="tar -zxf"

# Functions

tf()
{
 current_dir=$(pwd)
 ls -rtlh | awk -v dir="$current_dir" '{tot=tot + $5} END {print "Taille totale des fichiers de " dir " : " tot " Mo"}'
}

fullupg()
{
 commands=(update upgrade dist-upgrade full-upgrade autoclean autoremove)
 for cmd in "${commands[@]}";
 do
   sudo apt $cmd -y
 done
}

ext()
{
 if [ -f $1 ]
 then
   case $1 in
     *.tar.bz2)   tar xvjf $1    ;;
     *.tar.gz)    tar xvzf $1    ;;
     *.tar.xz)    tar xvJf $1    ;;
     *.bz2)       bunzip2 $1     ;;
     *.rar)       unar $1        ;;
     *.gz)        gunzip $1      ;;
     *.tar)       tar xvf $1     ;;
     *.tbz2)      tar xvjf $1    ;;
     *.tgz)       tar xvzf $1    ;;
     *.zip)       unzip $1       ;;
     *.Z)         uncompress $1  ;;
     *.7z)        7z x $1        ;;
     *.xz)        unxz $1        ;;
     *.exe)       cabextract $1  ;;
     *)           echo "'$1' cannot be extracted via ex()" ;;
   esac
 else
   echo "'$1' is not a valid file"
 fi
}

CrackPDF()
{
 echo "Password ?"
 read -s PW
 pdftk "$1" input_pw ${PW} output "$1".cracked
 mv "$1".cracked "$1"
}

meteo()
{
 curl https://fr.wttr.in/"${1}"
}

# To get rid of dashes, whitespaces and underscores in filenames
prettyName()
{
 for i in **/*
 do
   mv "$i" "`echo $i | sed 's/-/\./g; s/ /\./g; s/_/\./g'`" 2> /dev/null
 done
}

bkpfile()
{
 cp "$1" "${HOME}/Lab/BACKUPS/$(basename $1.$(date +%d%m%Y.%H%M%S))"
}

# Set 'man' colors
# https://www.howtogeek.com/683134/how-to-display-man-pages-in-color-on-linux/
man()
{
 export LESS_TERMCAP_md=$'\e[01;31m' \
 export LESS_TERMCAP_me=$'\e[0m' \
 export LESS_TERMCAP_us=$'\e[01;32m' \
 export LESS_TERMCAP_ue=$'\e[0m' \
 export LESS_TERMCAP_so=$'\e[45;93m' \
 export LESS_TERMCAP_se=$'\e[0m' \

 command man "$@"
}

viewLineNumber() { awk "NR == $1" $2; }

resizeWidth() { convert "$1" -resize "$2" "$1_$2" ; mv "$1_$2" "$1" ; }  # resizeWidth test.jpg 300

# Backups and Syncs

alias bkpsan="rsync -arv --delete ${HOME}/Lab/ /media/${USER}/2BAF-10A1/BACKUP/ --exclude 'GAMING' | tee -a ${HOME}/Lab/LOGS/bkpsan.$(date +%d%m%Y.%H%M%S).log"
alias bkpmax="rsync -arv --delete ${HOME}/Lab/ /media/${USER}/Maxtor/BACKUP/ --exclude 'GAMING' | tee -a ${HOME}/Lab/LOGS/bkpmax.$(date +%d%m%Y.%H%M%S).log"

export EDITOR=/usr/bin/vim
export PATH="$PATH:$(find ${HOME}/Lab/SHELLSCRIPTS/ -type d -printf ":%p")"

khal calendar
cd ${HOME}/Lab && ls -rtlh
