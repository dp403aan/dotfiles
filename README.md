# dotfiles

My current configuration :

* **WM:** i3
* **Terminal:** lxterminal
* **Shell:** bash
* **Editor:** vim
* **File Manager:** ranger, caja
* **PDF Viewer:** zathura
* **Image Viewer:** sxiv
* **Music Player:** cmus
* **Media Player:** vlc, mpv
* **Browser:** firefox
* **Calendar:** khal
