" Functions {{{
"""""""""""
" Resume at last position
function! s:CursorOldLine()
  if line("'\"") > 0 && line("'\"") <= line("$") |
    exe "normal g`\"" |
  endif
endfunction
autocmd BufReadPost * call s:CursorOldLine()

" Clear all registers
command! WipeReg for i in range(34,122) | silent! call setreg(nr2char(i), []) | endfor
" }}}
" Config {{{
set nocompatible                                            " Disable vi compatibilty
set clipboard+=unnamedplus                                  " Copy from everywhere
set encoding=utf-8                                          " Use utf-8 for encoding
set history=2000                                            " Keep 2000 lines of command line history
set ignorecase smartcase                                    " Overrides ignorecase if your pattern contains mixed case
set incsearch                                               " Highlight the searching string while typing (ctrl+g, ctrl+t)
set laststatus=2                                            " Always show statusline
set noswapfile                                              " Disable swap file
set number relativenumber                                   " Always show line numbers and relative numbers
set scrolloff=2                                             " Two lines from the top or bottom while scrolling
set shiftwidth=2                                            " Number of spaces inserted/removed with < and >
set showcmd                                                 " Show command being executed
set showmatch                                               " Cursor shows matching ) and }
set splitbelow splitright                                   " Keep current screen unmoved when creating new split screen
set t_Co=256                                                " Use 256 colours (Use this setting only if your terminal supports 256 colours)
set tabstop=4                                               " Number of spaces that a <Tab> in the file counts for
set wildmenu                                                " Enable auto completion menu after pressing TAB.
set foldmethod=marker                                       " Fold method

filetype plugin indent on                                   " Enable detection, plugins and indenting in one step
syntax on                                                   " Syntax highlight

hi Visual ctermfg=15 ctermbg=34                             " https://upload.wikimedia.org/wikipedia/commons/1/15/Xterm_256color_chart.svg
hi Folded ctermfg=15 ctermbg=0
hi Color1 ctermfg=15 ctermbg=25
hi Color2 ctermfg=15 ctermbg=33
hi Color3 ctermfg=15 ctermbg=24
hi NormalColor ctermfg=15 ctermbg=27
hi InsertColor ctermfg=15 ctermbg=202
hi VisualColor ctermfg=15 ctermbg=172

set statusline=
set statusline+=%#NormalColor#%{(mode()=='n')?'\ \ NORMAL\ ':''}
set statusline+=%#InsertColor#%{(mode()=='i')?'\ \ INSERT\ ':''}
set statusline+=%#VisualColor#%{(mode()=='v')?'\ \ VISUAL\ ':''}
set statusline+=%#Color1#\ %{strftime('%d-%m-%Y\ %H:%M')}
set statusline+=\ %#Color2#\ %<%f
set statusline+=\ %#Color3#%=%<%Y
set statusline+=\ %#Color2#\ %{&fileencoding}
set statusline+=\ %#Color1#\ Buffer\ #%n
set statusline+=\ %#NormalColor#\ %l/%L:%c
" }}}
" Mappings {{{
""""""""""
" Rebind leader key
let mapleader=","

" Map zz to esc
inoremap zz <esc>
vnoremap zz <esc>

" Toggle numbers and relative numbers
nmap <F6> :set number relativenumber!<cr>

" Disable arrow keys
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>

" Comment/Uncomment line(s) in files
nnoremap * :s/^/#/<cr><cr>
nnoremap µ :s/^#//<cr><cr>

vnoremap * :s/^/#/<cr>
vnoremap µ :s/^#//<cr>

" Comment/Uncomment line(s) in .vimrc
nnoremap <leader>n :s/^/"/<cr><cr>
nnoremap <leader>N :s/^"//<cr><cr>

vnoremap <leader>n :s/^/"/<cr>
vnoremap <leader>N :s/^"//<cr>

" Center the cursor vertically when moving to the next word during a search
nnoremap n nzz
nnoremap N Nzz

" Sudo to write
cmap w!! w !sudo tee % >/dev/null

" Add blank lines wherever you are
nnoremap <leader>o o<esc>0"_D
nnoremap <leader>O O<esc>0"_D

" Map gx due to issue with urxvt
nnoremap gx :!open <c-r><c-a>

" Map gh to 0
nnoremap gh 0

" Move among buffers with ctrl
nnoremap <c-n> :bnext<cr>
nnoremap <c-p> :bprev<cr>

" Paste last thing yanked, not deleted
nmap <leader>p "0p
nmap <leader>P "0P

" Toggle cursorline on and off
hi CursorLine ctermfg=0 ctermbg=43
nnoremap <leader>cl :set cursorline!<cr>

" Toggle cursor column on and off
hi CursorColumn ctermfg=0 ctermbg=43
nnoremap <leader>cc :set cursorcolumn!<cr>

" Toggle set list on and off
nnoremap <leader>sl :set list!<cr>

" Define line highlight color
hi LineHighlight ctermbg=0 ctermfg=43

" Highlight the current line
nnoremap <silent> <Leader>l :call matchadd('LineHighlight', '\%'.line('.').'l')<cr>

" Clear all the highlighted lines
nnoremap <silent> <Leader>C :call clearmatches()<cr>

" Open vimrc in a horizontal split
nnoremap <leader>v :sp $MYVIMRC<cr>

" Open wordlist in a horizontal split
nnoremap <leader>w :sp ~/.vim/misc/wordlist<cr>

" Open .bashrc in a horizontal split
nnoremap <leader>b :sp ~/.bashrc<cr>

" Delete marks
nnoremap <leader>dm :delmarks a-zA-Z0-9<cr>

" Convert inserted text to normal mode commands
inoremap <F2> <esc>u@.

" Insert output of khal calendar command
inoremap <leader>kc <C-r>=system('khal calendar')<cr>

" Chmod current script
nnoremap <leader>ch :!chmod u+x %<cr>

" Run current script
nnoremap <leader>x :!clear && ./%<cr>
" }}}
" Misc {{{
""""""
" Automated file templates
autocmd BufNewFile *.sh        0r ${HOME}/.vim/misc/BashHeader
autocmd BufNewFile *.html      0r ${HOME}/.vim/misc/index.html
autocmd BufNewFile LICENSE.txt 0r ${HOME}/.vim/misc/LICENSE.txt
autocmd BufNewFile *.md        0r ${HOME}/.vim/misc/README.md

autocmd FileType set local help relativenumber
autocmd VimLeavePre * :mksession! ~/stopped.vim

" YAML settings
autocmd FileType yaml,yml setlocal ai ts=2 sw=2 et  " autoindent tabstop shiftwidth expandtab

" Automatically removing all trailing whitespace
autocmd BufWritePre * :%s/\s\+$//e

" Abbreviations list
source ${HOME}/.vim/misc/wordlist

" Apply new configuration after .vimrc is saved
autocmd! BufWritePost .vimrc source %
" }}}
